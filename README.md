# springboot-kafka

#### 介绍
用于测试kafka连接

#### 软件架构
基于springboot框架


#### 使用说明

1.  将application.yml文件中的kafka连接地址修改为自己的

#### 特技

1.  [个人博客](https://blog.levnli.cn)
